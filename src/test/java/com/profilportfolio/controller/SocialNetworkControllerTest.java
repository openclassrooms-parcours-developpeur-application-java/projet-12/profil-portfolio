package com.profilportfolio.controller;

import com.profilportfolio.exception.ProfilNotFoundException;
import com.profilportfolio.exception.SocialNetworkNotFoundException;
import com.profilportfolio.exception.SocilaNetworkConflictException;
import com.profilportfolio.model.SocialNetworkEntity;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.repository.SocialNetworkRepository;
import com.profilportfolio.service.dto.SocialNetworkDTO;
import com.profilportfolio.service.mapper.SocialNetworkMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class SocialNetworkControllerTest {

    @Mock
    SocialNetworkRepository socialNetworkRepository;

    @Mock
    ProfilRepository profilRepository;

    @InjectMocks
    SocialNetworkController socialNetworkController;

    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllSocialNetworkByProfil() {

        List<SocialNetworkDTO> socialNetworkDTOListMock = new ArrayList<>();
        Integer profilId = 4;

        SocialNetworkDTO socialNetworkDTOMock1 = SocialNetworkDTO.builder().id(8).name("Nom social network 1")
                .url("URL social network 1").profilId(4).build();
        socialNetworkDTOListMock.add(socialNetworkDTOMock1);
        SocialNetworkDTO socialNetworkDTOMock2 = SocialNetworkDTO.builder().id(7).name("Nom social network 2")
                .url("URL social network 2").profilId(4).build();
        socialNetworkDTOListMock.add(socialNetworkDTOMock2);

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(socialNetworkRepository.findAllByProfilId(profilId)).thenReturn(
                SocialNetworkMapper.INSTANCE.toEntityList(socialNetworkDTOListMock));

        final List<SocialNetworkDTO> socialNetworkDTOList = socialNetworkController.getAllSocialNetworkByProfil(profilId);
        Assertions.assertEquals(socialNetworkDTOList.size(), 2);
        Assertions.assertEquals(socialNetworkDTOListMock.get(0).getProfilId(), 4);
        Assertions.assertEquals(socialNetworkDTOListMock.get(0).getId(), 8);

        when(profilRepository.existsById(profilId)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            socialNetworkController.getAllSocialNetworkByProfil(profilId);
        });
    }

    @Test
    void addSocialNetworkForProfil() {

        List<SocialNetworkDTO> socialNetworkDTOListMock = new ArrayList<>();
        List<SocialNetworkEntity> socialNetworkEntityList = new ArrayList<>();
        Integer profilId = 4;

        SocialNetworkDTO socialNetworkDTOMock1 = SocialNetworkDTO.builder().name("Nom social network 1")
                .url("URL social network 1").profilId(4).build();
        socialNetworkDTOListMock.add(socialNetworkDTOMock1);
        SocialNetworkDTO socialNetworkDTOMock2 = SocialNetworkDTO.builder().name("Nom social network 2")
                .url("URL social network 2").profilId(4).build();
        socialNetworkDTOListMock.add(socialNetworkDTOMock2);

        SocialNetworkEntity socialNetworkEntityMock1 = SocialNetworkEntity.builder().id(8).name("Nom social network 1")
                .url("URL social network 1").profilId(4).build();
        socialNetworkEntityList.add(socialNetworkEntityMock1);
        SocialNetworkEntity socialNetworkEntityMock2 = SocialNetworkEntity.builder().id(7).name("Nom social network 2")
                .url("URL social network 2").profilId(4).build();
        socialNetworkEntityList.add(socialNetworkEntityMock2);

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(socialNetworkRepository.socialNetworkExistsByName(socialNetworkDTOMock1.getName())).thenReturn(false);
        when(socialNetworkRepository.save(any())).thenReturn(socialNetworkEntityMock1);

        final List<SocialNetworkDTO> socialNetworkDTOList = socialNetworkController
                .addSocialNetworkForProfil(socialNetworkDTOListMock, profilId);
        Assertions.assertEquals(socialNetworkDTOList.size(), 2);
        Assertions.assertEquals(socialNetworkDTOList.get(0).getProfilId(), 4);
        Assertions.assertEquals(socialNetworkDTOList.get(0).getId(), 8);

        when(profilRepository.existsById(profilId)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            socialNetworkController.addSocialNetworkForProfil(socialNetworkDTOListMock, profilId);
        });

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(socialNetworkRepository.socialNetworkExistsByName(socialNetworkDTOMock1.getName())).thenReturn(true);
        Assertions.assertThrows(SocilaNetworkConflictException.class, () -> {
            socialNetworkController.addSocialNetworkForProfil(socialNetworkDTOListMock, profilId);
        });
    }

    @Test
    void upSocialNetwork() {

        SocialNetworkDTO socialNetworkDTOMock = SocialNetworkDTO.builder().id(8).name("Nom social network 1")
                .url("URL social network 1").profilId(4).build();
        SocialNetworkEntity socialNetworkEntityMock = SocialNetworkEntity.builder().id(8).name("Nom social network 1 - mise à jour")
                .url("URL social network 1").profilId(4).build();

        when(socialNetworkRepository.existsById(socialNetworkDTOMock.getId())).thenReturn(true);
        when(socialNetworkRepository.save(any())).thenReturn(socialNetworkEntityMock);

        final SocialNetworkDTO socialNetworkDTO = socialNetworkController.upSocialNetwork(socialNetworkDTOMock);
        Assertions.assertNotEquals(socialNetworkDTO.getName(), socialNetworkDTOMock.getName());
        Assertions.assertEquals(socialNetworkEntityMock.getName(), socialNetworkDTO.getName());

        when(socialNetworkRepository.existsById(socialNetworkDTOMock.getId())).thenReturn(false);
        Assertions.assertThrows(SocialNetworkNotFoundException.class, () -> {
            socialNetworkController.upSocialNetwork(socialNetworkDTO);
        });
    }

    @Test
    void delSocialNetwork() {

        Integer socialNetworkId = 7;

        when(socialNetworkRepository.deleteSocialNetworkById(socialNetworkId)).thenReturn(1);
        final String checkStatusSuccess = socialNetworkController.delSocialNetwork(socialNetworkId);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the social network");

        when(socialNetworkRepository.deleteSocialNetworkById(socialNetworkId)).thenReturn(0);
        final String checkStatusFailure = socialNetworkController.delSocialNetwork(socialNetworkId);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - social network not found");
    }
}
