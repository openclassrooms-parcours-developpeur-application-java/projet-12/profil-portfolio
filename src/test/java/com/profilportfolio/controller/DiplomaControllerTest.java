package com.profilportfolio.controller;

import com.profilportfolio.exception.DiplomaConflictException;
import com.profilportfolio.exception.DiplomaNotFoundException;
import com.profilportfolio.exception.ProfilNotFoundException;
import com.profilportfolio.model.DiplomaEntity;
import com.profilportfolio.proxy.MicroserviceMediaProxy;
import com.profilportfolio.repository.DiplomaRepository;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.service.dto.DiplomaDTO;
import com.profilportfolio.service.dto.ImageDTO;
import com.profilportfolio.service.mapper.DiplomaMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class DiplomaControllerTest {

    @Mock
    DiplomaRepository diplomaRepository;

    @Mock
    ProfilRepository profilRepository;

    @Mock
    MicroserviceMediaProxy microserviceMediaProxy;

    @InjectMocks
    DiplomaController diplomaController;

    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllDiplomaByProfil() throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        List<DiplomaDTO> diplomaDTOListMock = new ArrayList<>();
        List<ImageDTO> imageDTOList = new ArrayList<>();
        Integer profilId = 4;

        DiplomaDTO diplomaDTOMock1 = DiplomaDTO.builder().id(55).title("Titre diploma 1").city("City diploma 1")
                .school("School diploma 1").obtaining(new Timestamp(dateFormat.parse("2015-07-01").getTime()))
                .paragraph("Paragraph diploma 1").profilId(4).build();

        ImageDTO imageDTOMock = ImageDTO.builder().id(45).name("Nom image diploma 1").imagePath("Path image diploma 1").build();
        imageDTOList.add(imageDTOMock);
        diplomaDTOListMock.add(diplomaDTOMock1);

        DiplomaDTO diplomaDTOMock2 = DiplomaDTO.builder().id(5).title("Titre diploma 2").city("City diploma 2")
                .school("School diploma 2").obtaining(new Timestamp(dateFormat.parse("2020-07-01").getTime()))
                .paragraph("Paragraph diploma 2").profilId(4).build();
        diplomaDTOListMock.add(diplomaDTOMock2);

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(diplomaRepository.findAllByProfilId(profilId)).thenReturn(
                DiplomaMapper.INSTANCE.toEntityList(diplomaDTOListMock));
        when(microserviceMediaProxy.getListImageByDiploma(diplomaDTOMock1.getId())).thenReturn(imageDTOList);

        final List<DiplomaDTO> diplomaDTOList = diplomaController.getAllDiplomaByProfil(profilId);
        Assertions.assertEquals(diplomaDTOList.size(), 2);
        Assertions.assertEquals(diplomaDTOList.get(0).getProfilId(), 4);
        Assertions.assertEquals(diplomaDTOList.get(0).getId(), 5);
        Assertions.assertEquals(diplomaDTOList.get(1).getId(), 55);
        Assertions.assertEquals(diplomaDTOList.get(1).getImageDTOList().get(0).getId(), 45);

        when(profilRepository.existsById(profilId)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            diplomaController.getAllDiplomaByProfil(profilId);
        });
    }

    @Test
    void addDiplomaForProfil() throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        List<DiplomaDTO> diplomaDTOListMock = new ArrayList<>();
        Integer profilId = 4;

        DiplomaDTO diplomaDTOMock1 = DiplomaDTO.builder().title("Titre diploma 1").city("City diploma 1")
                .school("School diploma 1").obtaining(new Timestamp(dateFormat.parse("2015-07-01").getTime()))
                .paragraph("Paragraph diploma 1").profilId(4).build();
        diplomaDTOListMock.add(diplomaDTOMock1);

        DiplomaEntity diplomaEntityMock1 = DiplomaEntity.builder().id(55).title("Titre diploma 1").city("City diploma 1")
                .school("School diploma 1").obtaining(new Timestamp(dateFormat.parse("2015-07-01").getTime()))
                .paragraph("Paragraph diploma 1").profilId(4).build();

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(diplomaRepository.diplomaExistsByTitle(diplomaDTOMock1.getTitle())).thenReturn(false);
        when(diplomaRepository.save(any())).thenReturn(diplomaEntityMock1);

        final List<DiplomaDTO> diplomaDTOList = diplomaController.addDiplomaForProfil(diplomaDTOListMock, profilId);
        Assertions.assertEquals(diplomaDTOList.size(), 1);
        Assertions.assertEquals(diplomaDTOList.get(0).getProfilId(), 4);
        Assertions.assertEquals(diplomaDTOList.get(0).getId(), 55);

        when(profilRepository.existsById(profilId)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            diplomaController.addDiplomaForProfil(diplomaDTOListMock, profilId);
        });

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(diplomaRepository.diplomaExistsByTitle(diplomaDTOMock1.getTitle())).thenReturn(true);
        Assertions.assertThrows(DiplomaConflictException.class, () -> {
            diplomaController.addDiplomaForProfil(diplomaDTOListMock, profilId);
        });
    }


    @Test
    void upDiploma() throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        DiplomaDTO diplomaDTOMock = DiplomaDTO.builder().id(33).title("Titre diploma 1").city("City diploma 1")
                .school("School diploma 1").obtaining(new Timestamp(dateFormat.parse("2015-07-01").getTime()))
                .paragraph("Paragraph diploma 1").profilId(4).build();

        DiplomaEntity diplomaEntityMock = DiplomaEntity.builder().id(33).title("Titre diploma 1 - mise à jour")
                .city("City diploma 1").school("School diploma 1")
                .obtaining(new Timestamp(dateFormat.parse("2015-07-01").getTime()))
                .paragraph("Paragraph diploma 1").profilId(4).build();

        when(diplomaRepository.existsById(diplomaDTOMock.getId())).thenReturn(true);
        when(diplomaRepository.save(any())).thenReturn(diplomaEntityMock);

        final DiplomaDTO diplomaDTO = diplomaController.upDiploma(diplomaDTOMock);
        Assertions.assertNotEquals(diplomaDTO.getTitle(), diplomaDTOMock.getTitle());
        Assertions.assertEquals(diplomaEntityMock.getTitle(), diplomaDTO.getTitle());

        when(diplomaRepository.existsById(diplomaDTO.getId())).thenReturn(false);
        Assertions.assertThrows(DiplomaNotFoundException.class, () -> {
            diplomaController.upDiploma(diplomaDTOMock);
        });
    }

    @Test
    void delDiploma() {

        Integer diplomaId = 32;

        when(diplomaRepository.deleteDiplomaById(diplomaId)).thenReturn(1);
        final String checkStatusSuccess = diplomaController.delDiploma(diplomaId);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the diploma");

        when(diplomaRepository.deleteDiplomaById(diplomaId)).thenReturn(0);
        final String checkStatusFailure = diplomaController.delDiploma(diplomaId);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - diploma not found");
    }

}
