package com.profilportfolio.controller;

import com.profilportfolio.exception.ExperienceConflictException;
import com.profilportfolio.exception.ExperienceNotFoundException;
import com.profilportfolio.exception.ProfilNotFoundException;
import com.profilportfolio.model.ExperienceEntity;
import com.profilportfolio.proxy.MicroserviceMediaProxy;
import com.profilportfolio.repository.ExperienceRepository;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.service.dto.ExperienceDTO;
import com.profilportfolio.service.dto.ImageDTO;
import com.profilportfolio.service.mapper.ExperienceMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ExperienceControllerTest {

    @Mock
    ExperienceRepository experienceRepository;

    @Mock
    ProfilRepository profilRepository;

    @Mock
    MicroserviceMediaProxy microserviceMediaProxy;

    @InjectMocks
    ExperienceController experienceController;

    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllExperienceByProfil() throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        List<ExperienceDTO> experienceDTOListMock = new ArrayList<>();
        List<ImageDTO> imageDTOList = new ArrayList<>();
        Integer profilId = 6;

        ExperienceDTO experienceDTOMock1 = ExperienceDTO.builder().id(7).title("Titre experience 1")
                .company("Company experience 1").commencementDate(new Timestamp(dateFormat.parse("2010-05-06").getTime()))
                .stopDate(new Timestamp(dateFormat.parse("2011-05-06").getTime())).city("City experience 1")
                .paragraph("Paragraphe experience 1").profilId(6).build();

        ImageDTO imageDTOMock = ImageDTO.builder().id(45).name("Nom image experience 1").imagePath("Path image experience 1").build();
        imageDTOList.add(imageDTOMock);
        experienceDTOListMock.add(experienceDTOMock1);

        ExperienceDTO experienceDTOMock2 = ExperienceDTO.builder().id(8).title("Titre experience 2")
                .company("Company experience 2").commencementDate(new Timestamp(dateFormat.parse("2015-05-06").getTime()))
                .stopDate(new Timestamp(dateFormat.parse("2016-05-06").getTime())).city("City experience 2")
                .paragraph("Paragraphe experience 2").profilId(6).build();
        experienceDTOListMock.add(experienceDTOMock2);

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(experienceRepository.findAllByProfilId(profilId)).thenReturn(
                ExperienceMapper.INSTANCE.toEntityList(experienceDTOListMock));
        when(microserviceMediaProxy.getListImageByExperience(experienceDTOMock1.getId())).thenReturn(imageDTOList);

        final List<ExperienceDTO> experienceDTOList = experienceController.getAllExperienceByProfil(profilId);
        Assertions.assertEquals(experienceDTOList.size(), 2);
        Assertions.assertEquals(experienceDTOList.get(0).getProfilId(), 6);
        Assertions.assertEquals(experienceDTOList.get(0).getId(), 8);
        Assertions.assertEquals(experienceDTOList.get(1).getId(), 7);
        Assertions.assertEquals(experienceDTOList.get(1).getImageDTOList().get(0).getId(), 45);

        when(profilRepository.existsById(profilId)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            experienceController.getAllExperienceByProfil(profilId);
        });
    }

    @Test
    void addExperienceForProfil() throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        List<ExperienceDTO> experienceDTOListMock = new ArrayList<>();
        Integer profilId = 6;

        ExperienceDTO experienceDTOMock = ExperienceDTO.builder().title("Titre experience")
                .company("Company experience").commencementDate(new Timestamp(dateFormat.parse("2010-05-06").getTime()))
                .stopDate(new Timestamp(dateFormat.parse("2011-05-06").getTime())).city("City experience")
                .paragraph("Paragraphe experience").profilId(6).build();
        experienceDTOListMock.add(experienceDTOMock);

        ExperienceEntity experienceEntityMock = ExperienceEntity.builder().id(7).title("Titre experience")
                .company("Company experience").commencementDate(new Timestamp(dateFormat.parse("2010-05-06").getTime()))
                .stopDate(new Timestamp(dateFormat.parse("2011-05-06").getTime())).city("City experience")
                .paragraph("Paragraphe experience").profilId(6).build();

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(experienceRepository.experienceExistsByTitle(experienceDTOMock.getTitle())).thenReturn(false);
        when(experienceRepository.save(any())).thenReturn(experienceEntityMock);

        final List<ExperienceDTO> experienceDTOList = experienceController.addExperienceForProfil(experienceDTOListMock, profilId);
        Assertions.assertEquals(experienceDTOList.size(), 1);
        Assertions.assertEquals(experienceDTOList.get(0).getProfilId(), 6);
        Assertions.assertEquals(experienceDTOList.get(0).getId(), 7);

        when(profilRepository.existsById(profilId)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            experienceController.addExperienceForProfil(experienceDTOListMock, profilId);
        });

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(experienceRepository.experienceExistsByTitle(experienceDTOMock.getTitle())).thenReturn(true);
        Assertions.assertThrows(ExperienceConflictException.class, () -> {
            experienceController.addExperienceForProfil(experienceDTOListMock, profilId);
        });

    }

    @Test
    void upExperience() throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        ExperienceDTO experienceDTOMock = ExperienceDTO.builder().id(7).title("Titre experience")
                .company("Company experience").commencementDate(new Timestamp(dateFormat.parse("2010-05-06").getTime()))
                .stopDate(new Timestamp(dateFormat.parse("2011-05-06").getTime())).city("City experience")
                .paragraph("Paragraphe experience").profilId(6).build();

        ExperienceEntity experienceEntityMock = ExperienceEntity.builder().id(7).title("Titre experience - mise à four")
                .company("Company experience").commencementDate(new Timestamp(dateFormat.parse("2010-05-06").getTime()))
                .stopDate(new Timestamp(dateFormat.parse("2011-05-06").getTime())).city("City experience")
                .paragraph("Paragraphe experience").profilId(6).build();

        when(experienceRepository.existsById(experienceDTOMock.getId())).thenReturn(true);
        when(experienceRepository.save(any())).thenReturn(experienceEntityMock);

        final ExperienceDTO experienceDTO = experienceController.upExperience(experienceDTOMock);
        Assertions.assertNotEquals(experienceDTO.getTitle(), experienceDTOMock.getTitle());
        Assertions.assertEquals(experienceEntityMock.getTitle(), experienceDTO.getTitle());

        when(experienceRepository.existsById(experienceDTO.getId())).thenReturn(false);
        Assertions.assertThrows(ExperienceNotFoundException.class, () -> {
            experienceController.upExperience(experienceDTOMock);
        });
    }

    @Test
    void delExperience() {

        Integer experienceId = 32;

        when(experienceRepository.deleteExperienceById(experienceId)).thenReturn(1);
        final String checkStatusSuccess = experienceController.delExperience(experienceId);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the experience");

        when(experienceRepository.deleteExperienceById(experienceId)).thenReturn(0);
        final String checkStatusFailure = experienceController.delExperience(experienceId);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - experience not found");
    }

}
