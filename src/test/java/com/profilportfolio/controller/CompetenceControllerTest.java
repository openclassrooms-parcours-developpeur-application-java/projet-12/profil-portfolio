package com.profilportfolio.controller;

import com.profilportfolio.exception.CompetenceConflictException;
import com.profilportfolio.exception.CompetenceNotFoundException;
import com.profilportfolio.exception.ProfilNotFoundException;
import com.profilportfolio.model.CompetenceEntity;
import com.profilportfolio.repository.CompetenceRepository;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.service.dto.CompetenceDTO;
import com.profilportfolio.service.mapper.CompetenceMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import java.util.ArrayList;
import java.util.List;

class CompetenceControllerTest {

    @Mock
    CompetenceRepository competenceRepository;

    @Mock
    ProfilRepository profilRepository;

    @InjectMocks
    CompetenceController competenceController;

    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllCompetenceByProfil() {

        List<CompetenceDTO> competenceDTOListMock = new ArrayList<>();
        Integer profilId = 4;

        CompetenceDTO competenceDTOMock1 = CompetenceDTO.builder().id(8).name("Nom compétence 1").numberOfPracticeMonths(12)
                .level(55).profilId(4).build();
        competenceDTOListMock.add(competenceDTOMock1);
        CompetenceDTO competenceDTOMock2 = CompetenceDTO.builder().id(7).name("Nom compétence 2").numberOfPracticeMonths(22)
                .level(45).profilId(4).build();
        competenceDTOListMock.add(competenceDTOMock2);

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(competenceRepository.findAllByProfilId(profilId)).thenReturn(
                CompetenceMapper.INSTANCE.toEntityList(competenceDTOListMock));

        final List<CompetenceDTO> competenceDTOList = competenceController.getAllCompetenceByProfil(profilId);
        Assertions.assertEquals(competenceDTOList.size(), 2);
        Assertions.assertEquals(competenceDTOListMock.get(0).getProfilId(), 4);
        Assertions.assertEquals(competenceDTOListMock.get(0).getId(), 8);

        when(profilRepository.existsById(profilId)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            competenceController.getAllCompetenceByProfil(profilId);
        });
    }

    @Test
    void addCompetenceForProfil() {

        List<CompetenceDTO> competenceDTOListMock = new ArrayList<>();
        List<CompetenceEntity> competenceEntityListMock = new ArrayList<>();
        Integer profilId = 4;

        CompetenceDTO competenceDTOMock1 = CompetenceDTO.builder().name("Nom compétence 1").numberOfPracticeMonths(12)
                .level(55).profilId(4).build();
        competenceDTOListMock.add(competenceDTOMock1);

        CompetenceEntity competenceEntityMock1 = CompetenceEntity.builder().id(7).name("Nom compétence 1").numberOfPracticeMonths(12)
                .level(55).profilId(4).build();
        competenceEntityListMock.add(competenceEntityMock1);

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(competenceRepository.competenceExistsByName(competenceDTOMock1.getName())).thenReturn(false);
        when(competenceRepository.save(any())).thenReturn(competenceEntityMock1);

        final List<CompetenceDTO> competenceDTOList = competenceController.addCompetenceForProfil(competenceDTOListMock, profilId);
        Assertions.assertEquals(competenceDTOList.size(), 1);
        Assertions.assertEquals(competenceDTOListMock.get(0).getProfilId(), 4);
        Assertions.assertEquals(competenceDTOListMock.get(0).getId(), 7);

        when(profilRepository.existsById(profilId)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            competenceController.addCompetenceForProfil(competenceDTOListMock, profilId);
        });

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(competenceRepository.competenceExistsByName(competenceDTOMock1.getName())).thenReturn(true);
        Assertions.assertThrows(CompetenceConflictException.class, () -> {
            competenceController.addCompetenceForProfil(competenceDTOListMock, profilId);
        });
    }

    @Test
    void upCompetence() {

        CompetenceDTO competenceDTOMock = CompetenceDTO.builder().id(9).name("Nom compétence 1").numberOfPracticeMonths(12)
                .level(55).profilId(4).build();
        CompetenceEntity competenceEntityMock = CompetenceEntity.builder().id(9).name("Nom compétence - mise à jour").numberOfPracticeMonths(12)
                .level(55).profilId(4).build();

        when(competenceRepository.existsById(competenceDTOMock.getId())).thenReturn(true);
        when(competenceRepository.save(any())).thenReturn(competenceEntityMock);

        final CompetenceDTO competenceDTO = competenceController.upCompetence(competenceDTOMock);
        Assertions.assertNotEquals(competenceDTO.getName(), competenceDTOMock.getName());
        Assertions.assertEquals(competenceEntityMock.getName(), competenceDTO.getName());

        when(competenceRepository.existsById(competenceDTOMock.getId())).thenReturn(false);
        Assertions.assertThrows(CompetenceNotFoundException.class, () -> {
            competenceController.upCompetence(competenceDTOMock);
        });
    }

    @Test
    void delCompetence() {

        Integer competenceId = 5;

        when(competenceRepository.deleteCompetenceById(competenceId)).thenReturn(1);
        final String checkStatusSuccess = competenceController.delCompetence(competenceId);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the competence");

        when(competenceRepository.deleteCompetenceById(competenceId)).thenReturn(0);
        final String checkStatusFailure = competenceController.delCompetence(competenceId);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - competence not found");
    }
}
