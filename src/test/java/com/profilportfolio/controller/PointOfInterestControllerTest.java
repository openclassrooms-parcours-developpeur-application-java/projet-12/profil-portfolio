package com.profilportfolio.controller;

import com.profilportfolio.exception.PointOfInterestConflictException;
import com.profilportfolio.exception.PointOfInterestNotFoundException;
import com.profilportfolio.exception.ProfilNotFoundException;
import com.profilportfolio.model.PointOfInterestEntity;
import com.profilportfolio.model.TagEntity;
import com.profilportfolio.repository.PointOfInterestRepository;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.repository.TagRepository;
import com.profilportfolio.service.dto.PointOfInterestDTO;
import com.profilportfolio.service.dto.TagDTO;
import com.profilportfolio.service.mapper.PointOfInterestMapper;
import com.profilportfolio.service.mapper.TagMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class PointOfInterestControllerTest {

    @Mock
    PointOfInterestRepository pointOfInterestRepository;

    @Mock
    ProfilRepository profilRepository;

    @Mock
    TagRepository tagRepository;

    @InjectMocks
    PointOfInterestController pointOfInterestController;

    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllPointOfInterestByProfil() {

        List<PointOfInterestDTO> pointOfInterestDTOListMock = new ArrayList<>();
        List<TagDTO> tagDTOListMock = new ArrayList<>();
        Integer profilIdMock = 4;

        PointOfInterestDTO pointOfInterestDTOMock1 = PointOfInterestDTO.builder().id(5).name("Nom point of interest 1")
                .paragraph("Paragraphe point of interest 1").profilId(4).build();
        TagDTO tagDTOMock = TagDTO.builder().id(8).name("Nom tag 1").tagPath("Path tag 1").build();
        tagDTOListMock.add(tagDTOMock);
        pointOfInterestDTOListMock.add(pointOfInterestDTOMock1);

        PointOfInterestDTO pointOfInterestDTOMock2 = PointOfInterestDTO.builder().id(15).name("Nom point of interest 2")
                .paragraph("Paragraphe point of interest 2").profilId(4).build();
        pointOfInterestDTOListMock.add(pointOfInterestDTOMock2);

        when(profilRepository.existsById(profilIdMock)).thenReturn(true);
        when(pointOfInterestRepository.findAllByProfilId(profilIdMock)).thenReturn(
                PointOfInterestMapper.INSTANCE.toEntityList(pointOfInterestDTOListMock));
        when(tagRepository.findAllTagByPointOfInterest(pointOfInterestDTOMock1.getId())).thenReturn(
                TagMapper.INSTANCE.toEntityList(tagDTOListMock));

        final List<PointOfInterestDTO> pointOfInterestDTOList = pointOfInterestController.getAllPointOfInterestByProfil(profilIdMock);
        Assertions.assertEquals(pointOfInterestDTOList.size(), 2);
        Assertions.assertEquals(pointOfInterestDTOList.get(0).getProfilId(), 4);
        Assertions.assertEquals(pointOfInterestDTOList.get(0).getId(), 5);
        Assertions.assertEquals(pointOfInterestDTOList.get(0).getTagDTOList().get(0).getId(), 8);
        Assertions.assertEquals(pointOfInterestDTOList.get(1).getId(), 15);

        when(profilRepository.existsById(profilIdMock)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            pointOfInterestController.getAllPointOfInterestByProfil(profilIdMock);
        });

     }

    @Test
    void addPointOfInterestForProfil() {

        List<PointOfInterestDTO> pointOfInterestDTOListMock = new ArrayList<>();
        Integer profilIdMock = 4;

        PointOfInterestDTO pointOfInterestDTOMock = PointOfInterestDTO.builder().name("Nom point of interest 1")
                .paragraph("Paragraphe point of interest 1").profilId(4).build();
        pointOfInterestDTOListMock.add(pointOfInterestDTOMock);

        PointOfInterestEntity pointOfInterestEntityMock = PointOfInterestEntity.builder().id(85).name("Nom point of interest 1")
                .paragraph("Paragraphe point of interest 1").profilId(4).build();

        when(profilRepository.existsById(profilIdMock)).thenReturn(true);
        when(pointOfInterestRepository.pointOfInterestExistsByName(pointOfInterestDTOMock.getName())).thenReturn(false);
        when(pointOfInterestRepository.save(any())).thenReturn(pointOfInterestEntityMock);

        final List<PointOfInterestDTO> pointOfInterestDTOList = pointOfInterestController
                .addPointOfInterestForProfil(pointOfInterestDTOListMock, profilIdMock);
        Assertions.assertEquals(pointOfInterestDTOList.size(), 1);
        Assertions.assertEquals(pointOfInterestDTOList.get(0).getProfilId(), 4);
        Assertions.assertEquals(pointOfInterestDTOList.get(0).getId(), 85);

        when(profilRepository.existsById(profilIdMock)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            pointOfInterestController.addPointOfInterestForProfil(pointOfInterestDTOList, profilIdMock);
        });

        when(profilRepository.existsById(profilIdMock)).thenReturn(true);
        when(pointOfInterestRepository.pointOfInterestExistsByName(pointOfInterestDTOMock.getName())).thenReturn(true);
        Assertions.assertThrows(PointOfInterestConflictException.class, () -> {
            pointOfInterestController.addPointOfInterestForProfil(pointOfInterestDTOList, profilIdMock);
        });
    }

    @Test
    void addTagPointOfInterest() {

        List<TagDTO> tagDTOListMock = new ArrayList<>();
        Integer pointOfInterestIdMock = 4;

        TagDTO tagDTOMock = TagDTO.builder().name("Nom tag 1").tagPath("Path tag 1").build();
        tagDTOListMock.add(tagDTOMock);
        TagEntity tagEntityMock = TagEntity.builder().id(36).name("Nom tag 1").tagPath("Path tag 1").build();

        when(tagRepository.findByTagPath(tagDTOMock.getTagPath())).thenReturn(tagEntityMock);
        when(tagRepository.tagPointOfInterestExists(tagEntityMock.getId(), pointOfInterestIdMock)).thenReturn(false);
        when(tagRepository.joinTagToThePointOfInterest(tagEntityMock.getId(), pointOfInterestIdMock)).thenReturn(1);

        final List<TagDTO> tagDTOList = pointOfInterestController.addTagPointOfInterest(tagDTOListMock, pointOfInterestIdMock);
        for (TagDTO tagDTO : tagDTOList) {
            Assertions.assertEquals(tagDTO.getId(), 36);
        }

        when(tagRepository.findByTagPath(tagDTOMock.getTagPath())).thenReturn(null);
        when(pointOfInterestRepository.existsById(pointOfInterestIdMock)).thenReturn(true);
        when(tagRepository.save(any())).thenReturn(tagEntityMock);
        when(tagRepository.joinTagToThePointOfInterest(tagEntityMock.getId(), pointOfInterestIdMock)).thenReturn(1);

        final List<TagDTO> tagDTOList1 = pointOfInterestController.addTagPointOfInterest(tagDTOListMock, pointOfInterestIdMock);
        for (TagDTO tagDTO : tagDTOList1) {
            Assertions.assertEquals(tagDTO.getId(), 36);
        }
    }

    @Test
    void addTagPointOfInterestException() {

        List<TagDTO> tagDTOListMock = new ArrayList<>();
        Integer pointOfInterestIdMock = 4;

        TagDTO tagDTOMock = TagDTO.builder().name("Nom tag 1").tagPath("Path tag 1").build();
        tagDTOListMock.add(tagDTOMock);
        TagEntity tagEntityMock = TagEntity.builder().id(36).name("Nom tag 1").tagPath("Path tag 1").build();

        when(tagRepository.findByTagPath(tagDTOMock.getTagPath())).thenReturn(tagEntityMock);
        when(tagRepository.tagPointOfInterestExists(tagEntityMock.getId(), pointOfInterestIdMock)).thenReturn(true);
        Assertions.assertThrows(PointOfInterestConflictException.class, () -> {
            pointOfInterestController.addTagPointOfInterest(tagDTOListMock, pointOfInterestIdMock);
        });

        when(tagRepository.findByTagPath(tagDTOMock.getTagPath())).thenReturn(tagEntityMock);
        when(tagRepository.tagPointOfInterestExists(tagEntityMock.getId(), pointOfInterestIdMock)).thenReturn(false);
        when(tagRepository.joinTagToThePointOfInterest(tagEntityMock.getId(), pointOfInterestIdMock))
                .thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(PointOfInterestNotFoundException.class, () -> {
            pointOfInterestController.addTagPointOfInterest(tagDTOListMock, pointOfInterestIdMock);
        });

        when(tagRepository.findByTagPath(tagDTOMock.getTagPath())).thenReturn(null);
        when(tagRepository.tagPointOfInterestExists(tagEntityMock.getId(), pointOfInterestIdMock)).thenReturn(true);
        when(pointOfInterestRepository.existsById(pointOfInterestIdMock)).thenReturn(false);
        Assertions.assertThrows(PointOfInterestNotFoundException.class, () -> {
            pointOfInterestController.addTagPointOfInterest(tagDTOListMock, pointOfInterestIdMock);
        });
    }

    @Test
    void upPointOfInterest() {

        PointOfInterestDTO pointOfInterestDTOMock = PointOfInterestDTO.builder().id(33).name("Nom point of interest 1")
                .paragraph("Paragraphe point of interest 1").profilId(4).build();

        PointOfInterestEntity pointOfInterestEntityMock = PointOfInterestEntity.builder().id(33)
                .name("Nom point of interest 1 - mise à jour")
                .paragraph("Paragraphe point of interest 1").profilId(4).build();

        when(pointOfInterestRepository.existsById(pointOfInterestDTOMock.getId())).thenReturn(true);
        when(pointOfInterestRepository.save(any())).thenReturn(pointOfInterestEntityMock);

        final PointOfInterestDTO pointOfInterestDTO = pointOfInterestController.upPointOfInterest(pointOfInterestDTOMock);
        Assertions.assertNotEquals(pointOfInterestDTO.getName(), pointOfInterestDTOMock.getName());
        Assertions.assertEquals(pointOfInterestEntityMock.getName(), pointOfInterestDTO.getName());

        when(pointOfInterestRepository.existsById(pointOfInterestDTO.getId())).thenReturn(false);
        Assertions.assertThrows(PointOfInterestNotFoundException.class, () -> {
            pointOfInterestController.upPointOfInterest(pointOfInterestDTOMock);
        });
    }


    @Test
    void delPointOfInterest() {

        Integer pointOfInterestIdMock = 32;

        when(pointOfInterestRepository.deletePointOfInterestById(pointOfInterestIdMock)).thenReturn(1);
        final String checkStatusSuccess = pointOfInterestController.delPointOfInterest(pointOfInterestIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the point of interest");

        when(pointOfInterestRepository.deletePointOfInterestById(pointOfInterestIdMock)).thenReturn(0);
        final String checkStatusFailure = pointOfInterestController.delPointOfInterest(pointOfInterestIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - point of interest not found");
    }

    @Test
    void delTagByPointOfInterest() {

        Integer tagIdMock = 1;
        Integer pointOfInterestIdMock = 5;

        when(tagRepository.deleteTagForPointOfInterest(tagIdMock, pointOfInterestIdMock)).thenReturn(1);
        final String checkStatusSuccess = pointOfInterestController.delTagByPointOfInterest(tagIdMock, pointOfInterestIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the tag");

        when(tagRepository.deleteTagForPointOfInterest(tagIdMock, pointOfInterestIdMock)).thenReturn(0);
        final String checkStatusFailure = pointOfInterestController.delTagByPointOfInterest(tagIdMock, pointOfInterestIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - tag not found");
    }
}
