package com.profilportfolio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DiplomaNotFoundException extends RuntimeException {

    public DiplomaNotFoundException(String s) {
        super(s);
    }
}
