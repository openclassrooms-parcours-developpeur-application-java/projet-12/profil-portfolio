package com.profilportfolio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class CompetenceConflictException extends RuntimeException {

    public CompetenceConflictException(String s) {
        super(s);
    }
}
