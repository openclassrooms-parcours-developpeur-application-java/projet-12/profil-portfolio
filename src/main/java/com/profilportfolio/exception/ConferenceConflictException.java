package com.profilportfolio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ConferenceConflictException extends RuntimeException {

    public ConferenceConflictException(String s) {
        super(s);
    }
}
