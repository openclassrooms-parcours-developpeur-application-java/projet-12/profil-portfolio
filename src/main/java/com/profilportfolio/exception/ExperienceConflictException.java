package com.profilportfolio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ExperienceConflictException extends RuntimeException {

    public ExperienceConflictException(String s) {
        super(s);
    }
}
