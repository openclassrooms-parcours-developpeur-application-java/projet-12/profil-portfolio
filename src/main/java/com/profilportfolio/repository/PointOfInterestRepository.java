package com.profilportfolio.repository;

import com.profilportfolio.model.PointOfInterestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface PointOfInterestRepository extends JpaRepository<PointOfInterestEntity, Integer> {


    /**
     * OBTENIR TOUTES LES POINT OF INTEREST PAR L'ID DU PROFIL
     * @param profilId
     * @return
     */
    List<PointOfInterestEntity> findAllByProfilId(Integer profilId);


    /**
     * VERIFIE SI LE POINT OF INTEREST EXISTE DEJA
     * @param pointOfInterestName
     * @return
     */
    @Query(value = "SELECT COUNT(point_of_interest) > 0 FROM point_of_interest WHERE point_of_interest.name= :pointOfInterestName", nativeQuery = true)
    Boolean pointOfInterestExistsByName(@Param("pointOfInterestName") String pointOfInterestName);


    /**
     * SUPPRIMER LE POINT OF INTEREST PAR SON ID
     * @param pointOfInterestId
     * @return
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM point_of_interest" +
            " WHERE point_of_interest.id= :pointOfInterestId", nativeQuery = true)
    int deletePointOfInterestById(@Param("pointOfInterestId") Integer pointOfInterestId);

}
