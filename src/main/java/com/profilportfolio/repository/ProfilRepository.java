package com.profilportfolio.repository;

import com.profilportfolio.model.ProfilEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ProfilRepository extends JpaRepository<ProfilEntity, Integer> {


    /**
     * VERIFIE SI LE PROFIL EXISTE DEJA POUR LE MEME EMAIL
     * @param profilByEmail
     * @return
     */
    @Query(value = "SELECT COUNT(profil) > 0 FROM profil WHERE profil.email= :profilByEmail", nativeQuery = true)
    Boolean profilExistsByEmail(@Param("profilByEmail") String profilByEmail);


    /**
     * VERIFIE SI LE PROFIL EXISTE DEJA POUR LE MEME NUMERO DE TELEPHONE
     * @param profilByPhoneNumber
     * @return
     */
    @Query(value = "SELECT COUNT(profil) > 0 FROM profil WHERE profil.phone_number= :profilByPhoneNumber", nativeQuery = true)
    Boolean profilExistsByPhoneNumber(@Param("profilByPhoneNumber") String profilByPhoneNumber);


    /**
     * SUPPRIMER LE PROFIL PAR SON ID
     * @param profilId
     * @return
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM profil" +
            " WHERE profil.id= :profilId", nativeQuery = true)
    int deleteProfilById(@Param("profilId") Integer profilId);

}
