package com.profilportfolio.repository;

import com.profilportfolio.model.SocialNetworkEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface SocialNetworkRepository extends JpaRepository<SocialNetworkEntity, Integer> {


    /**
     * OBTENIR TOUTES LES SOCIAL NETWORK PAR L'ID DU PROFIL
     * @param profilId
     * @return
     */
    List<SocialNetworkEntity> findAllByProfilId(Integer profilId);


    /**
     * VERIFIE SI SOCIAL NETWORK EXISTE DEJA
     * @param socialNetworkName
     * @return
     */
    @Query(value = "SELECT COUNT(social_network) > 0 FROM social_network WHERE social_network.name= :socialNetworkName", nativeQuery = true)
    Boolean socialNetworkExistsByName(@Param("socialNetworkName") String socialNetworkName);


    /**
     * SUPPRIMER SOCIAL NETWORK PAR SON ID
     * @param socialNetworkId
     * @return
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM social_network" +
            " WHERE social_network.id= :socialNetworkId", nativeQuery = true)
    int deleteSocialNetworkById(@Param("socialNetworkId") Integer socialNetworkId);

}
