package com.profilportfolio.repository;

import com.profilportfolio.model.ExperienceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ExperienceRepository extends JpaRepository<ExperienceEntity, Integer> {


    /**
     * OBTENIR TOUTES LES EXPERIENCES PAR L'ID DU PROFIL
     * @param profilId
     * @return
     */
    List<ExperienceEntity> findAllByProfilId(Integer profilId);


    /**
     * VERIFIE SI L'EXPEREINCE EXISTE DEJA
     * @param experienceTitle
     * @return
     */
    @Query(value = "SELECT COUNT(experience) > 0 FROM experience WHERE experience.title= :experienceTitle", nativeQuery = true)
    Boolean experienceExistsByTitle(@Param("experienceTitle") String experienceTitle);


    /**
     * SUPPRIMER L'EXPERIENCE PAR SON ID
     * @param experienceId
     * @return
     */
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM experience" +
            " WHERE experience.id= :experienceId", nativeQuery = true)
    int deleteExperienceById(@Param("experienceId") Integer experienceId);

}
