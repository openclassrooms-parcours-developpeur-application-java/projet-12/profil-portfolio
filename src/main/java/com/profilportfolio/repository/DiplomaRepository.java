package com.profilportfolio.repository;

import com.profilportfolio.model.DiplomaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface DiplomaRepository extends JpaRepository<DiplomaEntity, Integer> {


    /**
     * OBTENIR TOUTES LES DIPLOMAS PAR L'ID DU PROFIL
     * @param profilId
     * @return
     */
    List<DiplomaEntity> findAllByProfilId(Integer profilId);


    /**
     * VERIFIE SI LE DIPLOMA EXISTE DEJA
     * @param diplomaTitle
     * @return
     */
    @Query(value = "SELECT COUNT(diploma) > 0 FROM diploma WHERE diploma.title= :diplomaTitle", nativeQuery = true)
    Boolean diplomaExistsByTitle(@Param("diplomaTitle") String diplomaTitle);


    /**
     * SUPPRIMER LE DIPLOMA PAR SON ID
     * @param diplomaId
     * @return
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM diploma" +
            " WHERE diploma.id= :diplomaId", nativeQuery = true)
    int deleteDiplomaById(@Param("diplomaId") Integer diplomaId);

}
