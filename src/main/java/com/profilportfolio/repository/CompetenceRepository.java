package com.profilportfolio.repository;

import com.profilportfolio.model.CompetenceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface CompetenceRepository extends JpaRepository<CompetenceEntity, Integer> {


    /**
     * OBTENIR TOUTES LES COMPETENCES PAR L'ID DU PROFIL
     * @param profilId
     * @return
     */
    List<CompetenceEntity> findAllByProfilId(Integer profilId);


    /**
     * VERIFIE SI LA COMPETENCE EXISTE DEJA
     * @param competenceName
     * @return
     */
    @Query(value = "SELECT COUNT(competence) > 0 FROM competence WHERE competence.name= :competenceName", nativeQuery = true)
    Boolean competenceExistsByName(@Param("competenceName") String competenceName);


    /**
     * SUPPRIMER LA COMPETENCE PAR SON ID
     * @param competenceId
     * @return
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM competence" +
            " WHERE competence.id= :competenceId", nativeQuery = true)
    int deleteCompetenceById(@Param("competenceId") Integer competenceId);

}
