package com.profilportfolio.repository;

import com.profilportfolio.model.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<TagEntity, Integer> {


    /**
     * OBTENIR TOUTES LES TAGS PAR L'ID POINT OF INTEREST
     * @param pointOfInterestId
     * @return
     */
    @Query(value = "SELECT DISTINCT tag.* FROM tag" +
            " INNER JOIN tag_pointofinterest ON  tag_pointofinterest.point_of_interest_id= :pointOfInterestId" +
            " WHERE tag.id=tag_pointofinterest.tag_id", nativeQuery = true)
    List<TagEntity> findAllTagByPointOfInterest(@Param("pointOfInterestId") Integer pointOfInterestId);


    /**
     * VERIFIE SI LE LIEN ENTRE LA TABLE TAG ET POINT OF INTEREST EXISTE DEJA
     * @param tagId
     * @param pointOfInterestId
     * @return
     */
    @Query(value = "SELECT count(tag_pointofinterest) > 0 FROM tag_pointofinterest" +
            " WHERE tag_pointofinterest.tag_id= :tagId AND tag_pointofinterest.point_of_interest_id= :pointOfInterestId", nativeQuery = true)
    Boolean tagPointOfInterestExists(@Param("tagId") Integer tagId, @Param("pointOfInterestId") Integer pointOfInterestId);


    /**
     * LIER DANS LA TABLE DE JOINTURE LE TAG A POINT OF INTEREST
     * @param tagId
     * @param pointOfInterestId
     */
    @Modifying @Transactional
    @Query(value = "INSERT INTO tag_pointofinterest (tag_id, point_of_interest_id)" +
            " VALUES (:tag_id, :pointOfInterestId)", nativeQuery = true)
    int joinTagToThePointOfInterest(@Param("tag_id") Integer tagId, @Param("pointOfInterestId") Integer pointOfInterestId);


    /**
     * SUPPRIMER LA JOINTURE ENTRE UN TAG ET POINT OF INTEREST
     * @param tagId
     * @param pointOfInterestId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM tag_pointofinterest" +
            " WHERE tag_id= :tagId AND point_of_interest_id= :pointOfInterestId", nativeQuery = true)
    int deleteTagForPointOfInterest(@Param("tagId") Integer tagId, @Param("pointOfInterestId") Integer pointOfInterestId);


    /**
     * OBTENIR LA RESOURCE PAR SON PATH
     * @param tagPath
     * @return
     */
    TagEntity findByTagPath(String tagPath);
}
