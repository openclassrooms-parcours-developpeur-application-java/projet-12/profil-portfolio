package com.profilportfolio.model;

import lombok.*;

import javax.persistence.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "conference")
public class ConferenceEntity {

    @Id @GeneratedValue
    private Integer id;

    private String name;

    private String summarize;

    @Column(name = "profil_id")
    private Integer profilId;

}
