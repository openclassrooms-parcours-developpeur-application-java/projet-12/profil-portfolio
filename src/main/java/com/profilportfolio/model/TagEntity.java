package com.profilportfolio.model;

import lombok.*;
import javax.persistence.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "tag")
public class TagEntity {

    @Id @GeneratedValue
    private Integer id;

    private String name;

    @Column(name = "tag_path")
    private String tagPath;

}
