package com.profilportfolio.model;

import lombok.*;

import javax.persistence.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "social_network")
public class SocialNetworkEntity {

    @Id @GeneratedValue
    private Integer id;

    private String name;

    private String url;

    @Column(name = "profil_id")
    private Integer profilId;

}
