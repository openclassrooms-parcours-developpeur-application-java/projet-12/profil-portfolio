package com.profilportfolio.model;

import lombok.*;
import javax.persistence.*;
import java.sql.Timestamp;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "profil")
public class ProfilEntity {

    @Id @GeneratedValue
    private Integer id;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "first_name")
    private String firstName;

    private String adress;

    @Column(name = "date_of_birth")
    private Timestamp dateOfBirth;

    @Column(name = "phone_number")
    private String phoneNumber;

    private String email;

}
