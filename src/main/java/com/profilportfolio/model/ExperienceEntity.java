package com.profilportfolio.model;

import lombok.*;
import javax.persistence.*;
import java.sql.Timestamp;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "experience")
public class ExperienceEntity {

    @Id @GeneratedValue
    private Integer id;

    private String title;

    private String company;

    @Column(name = "commencement_date")
    private Timestamp commencementDate;

    @Column(name = "stop_date")
    private Timestamp stopDate;

    private String city;

    private String paragraph;

    @Column(name = "profil_id")
    private Integer profilId;

}
