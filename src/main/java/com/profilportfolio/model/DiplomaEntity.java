package com.profilportfolio.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "diploma")
public class DiplomaEntity {

    @Id @GeneratedValue
    private Integer id;

    private String title;

    private Timestamp obtaining;

    private String school;

    private String city;

    private String paragraph;

    @Column(name = "profil_id")
    private Integer profilId;

}
