package com.profilportfolio.model;

import lombok.*;

import javax.persistence.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "point_of_interest")
public class PointOfInterestEntity {

    @Id @GeneratedValue
    private Integer id;

    private String name;

    private String paragraph;

    @Column(name = "profil_id")
    private Integer profilId;

}
