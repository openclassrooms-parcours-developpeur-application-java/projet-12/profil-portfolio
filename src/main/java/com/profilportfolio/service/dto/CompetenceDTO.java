package com.profilportfolio.service.dto;

import lombok.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class CompetenceDTO {

    private Integer id;

    private String name;

    private Integer level;

    private Integer numberOfPracticeMonths;

    private Integer profilId;

}
