package com.profilportfolio.service.dto;

import lombok.*;
import java.sql.Timestamp;
import java.util.List;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class ExperienceDTO {

    private Integer id;

    private String title;

    private String company;

    private Timestamp commencementDate;

    private Timestamp stopDate;

    private String city;

    private String paragraph;

    private Integer profilId;

    private List<ImageDTO> imageDTOList;

}
