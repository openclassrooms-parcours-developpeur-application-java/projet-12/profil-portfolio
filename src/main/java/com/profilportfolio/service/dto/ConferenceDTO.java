package com.profilportfolio.service.dto;

import lombok.*;
import java.util.List;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class ConferenceDTO {

    private Integer id;

    private String name;

    private String summarize;

    private Integer profilId;

    private List<ImageDTO> imageDTOList;

    private List<VideoDTO> videoDTOList;

}
