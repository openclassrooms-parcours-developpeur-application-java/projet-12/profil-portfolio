package com.profilportfolio.service.dto;

import lombok.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class SocialNetworkDTO {

    private Integer id;

    private String name;

    private String url;

    private Integer profilId;

}
