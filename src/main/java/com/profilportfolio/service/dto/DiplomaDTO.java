package com.profilportfolio.service.dto;

import lombok.*;

import java.sql.Timestamp;
import java.util.List;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class DiplomaDTO {

    private Integer id;

    private String title;

    private Timestamp obtaining;

    private String school;

    private String city;

    private String paragraph;

    private Integer profilId;

    private List<ImageDTO> imageDTOList;

}
