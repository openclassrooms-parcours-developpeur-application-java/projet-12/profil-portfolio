package com.profilportfolio.service.mapper;

import com.profilportfolio.model.ConferenceEntity;
import com.profilportfolio.service.dto.ConferenceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface ConferenceMapper {

    ConferenceMapper INSTANCE = Mappers.getMapper( ConferenceMapper.class );

    ConferenceDTO toDTO ( ConferenceEntity conferenceEntity );

    List<ConferenceDTO> toDTOList ( List<ConferenceEntity> conferenceEntityList );

    ConferenceEntity toEntity ( ConferenceDTO conferenceDTO );

    List<ConferenceEntity> toEntityList ( List<ConferenceDTO> conferenceDTOList );

}
