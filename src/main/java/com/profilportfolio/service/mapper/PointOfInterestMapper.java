package com.profilportfolio.service.mapper;

import com.profilportfolio.model.PointOfInterestEntity;
import com.profilportfolio.service.dto.PointOfInterestDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface PointOfInterestMapper {

    PointOfInterestMapper INSTANCE = Mappers.getMapper( PointOfInterestMapper.class );

    PointOfInterestDTO toDTO ( PointOfInterestEntity pointOfInterestEntity );

    List<PointOfInterestDTO> toDTOList ( List<PointOfInterestEntity> pointOfInterestEntityList );

    PointOfInterestEntity toEntity ( PointOfInterestDTO pointOfInterestDTO );

    List<PointOfInterestEntity> toEntityList ( List<PointOfInterestDTO> pointOfInterestDTOList );

}
