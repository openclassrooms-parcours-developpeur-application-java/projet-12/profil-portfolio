package com.profilportfolio.service.mapper;

import com.profilportfolio.model.ExperienceEntity;
import com.profilportfolio.service.dto.ExperienceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface ExperienceMapper {

    ExperienceMapper INSTANCE = Mappers.getMapper( ExperienceMapper.class );

    ExperienceDTO toDTO ( ExperienceEntity experienceEntity );

    List<ExperienceDTO> toDTOList ( List<ExperienceEntity> experienceEntityList );

    ExperienceEntity toEntity ( ExperienceDTO experienceDTO );

    List<ExperienceEntity> toEntityList ( List<ExperienceDTO> experienceDTOList );

}
