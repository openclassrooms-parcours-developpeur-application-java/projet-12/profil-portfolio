package com.profilportfolio.service.mapper;

import com.profilportfolio.model.CompetenceEntity;
import com.profilportfolio.service.dto.CompetenceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface CompetenceMapper {

    CompetenceMapper INSTANCE = Mappers.getMapper( CompetenceMapper.class );

    CompetenceDTO toDTO ( CompetenceEntity competenceEntity );

    List<CompetenceDTO> toDTOList ( List<CompetenceEntity> competenceEntityList );

    CompetenceEntity toEntity ( CompetenceDTO competenceDTO );

    List<CompetenceEntity> toEntityList ( List<CompetenceDTO> competenceDTOList );

}
