package com.profilportfolio.controller;

import com.profilportfolio.exception.ProfilNotFoundException;
import com.profilportfolio.exception.SocialNetworkNotFoundException;
import com.profilportfolio.exception.SocilaNetworkConflictException;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.repository.SocialNetworkRepository;
import com.profilportfolio.service.dto.SocialNetworkDTO;
import com.profilportfolio.service.mapper.SocialNetworkMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES SOCIAL NETWORK")
@RestController
public class SocialNetworkController {

    private static final Logger logger = LoggerFactory.getLogger(SocialNetworkController.class);

    @Autowired
    private SocialNetworkRepository socialNetworkRepository;

    @Autowired
    private ProfilRepository profilRepository;

                                            /* ===================================== */
                                            /* ================ GET ================ */
                                            /* ===================================== */

    /* ----------------------------------- GET ALL SOCIAL NETWORK BY PROFIL ----------------------------------- */
    @ApiOperation(value = "GET ALL SOCIAL NETWORK BY PROFIL")
    @GetMapping(value = "/profil/socialnetwork/get-all-socialnetwork/{profilId}")
    public List<SocialNetworkDTO> getAllSocialNetworkByProfil(@PathVariable Integer profilId) {

        List<SocialNetworkDTO> socialNetworkDTOList = new ArrayList<>();

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilId);

        if (profilExists) {

            try {

                /**
                 * OBTENIR TOUTES LES SOCIAL NETWORK PAR L'ID PROFIL
                 * @see SocialNetworkRepository#findAllByProfilId(Integer)
                 */
                socialNetworkDTOList = SocialNetworkMapper.INSTANCE.toDTOList(
                        socialNetworkRepository.findAllByProfilId(profilId));

            } catch (Exception pEX) {
                logger.error(String.valueOf(pEX));
            }

        } else {
            throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                    " - Cannot retrieve social network related to the profil");
        }
        return socialNetworkDTOList;
    }


                                            /* ===================================== */
                                            /* ================ ADD ================ */
                                            /* ===================================== */

    /* ------------------------------------------ ADD SOCIAL NETWORK ------------------------------------------ */
    @ApiOperation(value = "ADD SOCIAL NETWORK FOR PROFIL")
    @PostMapping(value = "/profil/socialnetwork/add-socialnetwork/{profilId}")
    public List<SocialNetworkDTO> addSocialNetworkForProfil(@RequestBody List<SocialNetworkDTO> socialNetworkDTOList,
                                                            @PathVariable Integer profilId) {

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilId);

        if (profilExists) {

            for (SocialNetworkDTO socialNetworkDTO : socialNetworkDTOList) {

                /** J'AJOUTE A SOCIAL NETWORK L'ID DU PROFIL AUQUEL CELUI-CI EST LIE */
                socialNetworkDTO.setProfilId(profilId);

                /** @see SocialNetworkRepository#socialNetworkExistsByName(String) */
                Boolean socialNetworkExistsByName = socialNetworkRepository.socialNetworkExistsByName(socialNetworkDTO.getName());

                if (!socialNetworkExistsByName) {

                    /** J'AJOUTE SOCIAL NETWORK, PUIS JE RECUPERE SON IDENTIFIANT */
                    socialNetworkDTO.setId(SocialNetworkMapper.INSTANCE.toDTO(
                            socialNetworkRepository.save(SocialNetworkMapper.INSTANCE.toEntity(socialNetworkDTO))).getId());

                } else {
                    throw new SocilaNetworkConflictException("The social network for the following name '" + socialNetworkDTO.getName() +
                            "' has already been added");
                }
            }

        } else {
            throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                    " - Cannot add social network related to the profile");
        }
        return socialNetworkDTOList;
    }


                                            /* ===================================== */
                                            /* ============== UPDATE =============== */
                                            /* ===================================== */

    /* -------------------------------------------- UP SOCIAL NETWORK -------------------------------------------- */
    @ApiOperation(value = "UP SOCIAL NETWORK")
    @PutMapping(value = "/profil/socialnetwork/up-socialnetwork")
    public SocialNetworkDTO upSocialNetwork(@RequestBody SocialNetworkDTO socialNetworkDTO) {

        /**
         * JE VERIFIE SI SOCIAL NETWORK EXISTE
         */
        Boolean socialNetworkExists= socialNetworkRepository.existsById(socialNetworkDTO.getId());

        if (socialNetworkExists) {

             try {

                 socialNetworkDTO = SocialNetworkMapper.INSTANCE.toDTO(
                         socialNetworkRepository.save(SocialNetworkMapper.INSTANCE.toEntity(socialNetworkDTO)));

             } catch (Exception pEX) {
                 logger.error("{}", String.valueOf(pEX));
             }

        } else {
            logger.error("Update failure - social network : '" + socialNetworkDTO.getName() + "' - Is not found");
            throw new SocialNetworkNotFoundException("Update failure - social network : '" + socialNetworkDTO.getName() +
                    "' - Is not found");
        }
        return socialNetworkDTO;
    }


                                            /* ======================================= */
                                            /* =============== DELETE  =============== */
                                            /* ======================================= */

    /* ------------------------------------- DEL SOCIAL NETWORK ------------------------------------- */
    @ApiOperation(value = "DEL SOCIAL NETWORK")
    @DeleteMapping(value = "/profil/socialnetwork/del-socialnetwork/{socialnetworkId}")
    public String delSocialNetwork(@PathVariable Integer socialnetworkId) {

        String massageStatus = "";

        try {

            /** @see SocialNetworkRepository#deleteSocialNetworkById(Integer) */
            Integer check = socialNetworkRepository.deleteSocialNetworkById(socialnetworkId);
            if (check == 0) {
                massageStatus = "Failed deletion - social network not found";
            } else {
                massageStatus = "Successful deletion of the social network";
            }
        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

}
