package com.profilportfolio.controller;

import com.profilportfolio.exception.ProfilConflictException;
import com.profilportfolio.exception.ProfilNotFoundException;
import com.profilportfolio.proxy.MicroserviceMediaProxy;
import com.profilportfolio.repository.*;
import com.profilportfolio.service.dto.*;
import com.profilportfolio.service.mapper.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Collections;
import java.util.Comparator;

@Api(description = "API POUR LES OPERATIONS CRUD SUR LES PROFILS")
@RestController
public class ProfilController {

    private static final Logger logger = LoggerFactory.getLogger(ProfilController.class);

    @Autowired
    private ProfilRepository profilRepository;
    
    @Autowired
    private CompetenceRepository competenceRepository;
    
    @Autowired
    private ConferenceRepository conferenceRepository;
    
    @Autowired
    private DiplomaRepository diplomaRepository;
    
    @Autowired
    private ExperienceRepository experienceRepository;
    
    @Autowired
    private PointOfInterestRepository pointOfInterestRepository;
    
    @Autowired
    private SocialNetworkRepository socialNetworkRepository;
    
    @Autowired
    private TagRepository tagRepository;
    
    @Autowired
    private MicroserviceMediaProxy microserviceMediaProxy;


                                        /* ===================================== */
                                        /* ================ GET ================ */
                                        /* ===================================== */

    /* --------------------------------------- GET PROFIL ---------------------------------------- */
    @ApiOperation(value = "GET ONLY PROFIL")
    @GetMapping(value = "/profil/profil/get-only-profil")
    public ProfilDTO getProfil() {

        ProfilDTO profilDTO = new ProfilDTO();

        try {

            /**
             * OBTENIR LE PROFIL
             * @see ProfilRepository
             * */
            profilDTO = ProfilMapper.INSTANCE.toDTO(profilRepository.findAll().get(0));

        } catch (Exception pEX) {
            logger.error("{}", String.valueOf(pEX));
        }
        return profilDTO;
    }

    /* --------------------------------------- GET FUll PROFIL ---------------------------------------- */
    @ApiOperation(value = "GET FULL PROFIL")
    @GetMapping(value = "/profil/profil/get-full-profil")
    public ProfilDTO getFullProfil() {

        ProfilDTO profilDTO = new ProfilDTO();

        try {

            /**
             * OBTENIR LE PROFIL
             * @see ProfilRepository
             * */
            profilDTO = ProfilMapper.INSTANCE.toDTO(profilRepository.findAll().get(0));

            /**
             * OBTENIR LES COMPETENCES BY ID PROFIL
             * @see CompetenceRepository#findAllByProfilId(Integer)
             */
            profilDTO.setCompetenceDTOList(CompetenceMapper.INSTANCE.toDTOList(
                    competenceRepository.findAllByProfilId(profilDTO.getId())));

            /**
             * OBTENIR LES CONFERENCES BY ID PROFIL
             * @see ConferenceRepository#findAllByProfilId(Integer)
             */
            profilDTO.setConferenceDTOList(ConferenceMapper.INSTANCE.toDTOList(
                    conferenceRepository.findAllByProfilId(profilDTO.getId())));

            for (ConferenceDTO conferenceDTO : profilDTO.getConferenceDTOList()) {

                /**
                 * OBTENIR LES VIDEOS BY ID CONFERENCES
                 * @see MicroserviceMediaProxy#getListVideoByConference(Integer)
                 */
                conferenceDTO.setVideoDTOList(microserviceMediaProxy.getListVideoByConference(conferenceDTO.getId()));

                /**
                 * OBTENIR LES IMAGES BY ID CONFERENCES
                 * @see MicroserviceMediaProxy#getListImageByConference(Integer)
                 */
                conferenceDTO.setImageDTOList(microserviceMediaProxy.getListImageByConference(conferenceDTO.getId()));
            }

            /**
             * OBTENIR LES DIPLOMES BY ID PROFIL
             * @see DiplomaRepository#findAllByProfilId(Integer)
             */
            profilDTO.setDiplomaDTOList(DiplomaMapper.INSTANCE.toDTOList(
                    diplomaRepository.findAllByProfilId(profilDTO.getId())));

            /**
             * TRIER LES DIPLOMES PAR ORDRE DECROISSANT EN FONCTION DE LEURS DATES
             */
            Collections.sort(profilDTO.getDiplomaDTOList(), new Comparator<DiplomaDTO>() {
                @Override
                public int compare(DiplomaDTO o1, DiplomaDTO o2) {
                    return o2.getObtaining().compareTo(o1.getObtaining());
                }
            });

            for (DiplomaDTO diplomaDTO : profilDTO.getDiplomaDTOList()) {

                /**
                 * OBTENIR LES IMAGES BY ID DIPLOMA
                 * @see MicroserviceMediaProxy#getListImageByDiploma(Integer)
                 */
                diplomaDTO.setImageDTOList(microserviceMediaProxy.getListImageByDiploma(diplomaDTO.getId()));
            }

            /**
             * OBTENIR LES EXPERIENCES BY ID PROFIL
             * @see ExperienceRepository#findAllByProfilId(Integer)
             */
            profilDTO.setExperienceDTOList(ExperienceMapper.INSTANCE.toDTOList(
                    experienceRepository.findAllByProfilId(profilDTO.getId())));

            /**
             * TRIER LES EXPERIENCES PAR ORDRE DECROISSANT EN FONCTION DE LEURS DATES
             */
            Collections.sort(profilDTO.getExperienceDTOList(), new Comparator<ExperienceDTO>() {
                @Override
                public int compare(ExperienceDTO o1, ExperienceDTO o2) {
                    return o2.getCommencementDate().compareTo(o1.getCommencementDate());
                }
            });

            for (ExperienceDTO experienceDTO : profilDTO.getExperienceDTOList()) {

                /**
                 * OBTENIR LES IMAGES BY ID EXPERIENCE
                 * @see MicroserviceMediaProxy#getListImageByExperience(Integer)
                 */
                experienceDTO.setImageDTOList(microserviceMediaProxy.getListImageByExperience(experienceDTO.getId()));
            }

            /**
             * OBTENIR LES POINT OF INTEREST BY ID PROFIL
             * @see PointOfInterestRepository#findAllByProfilId(Integer)
             */
            profilDTO.setPointOfInterestDTOList(PointOfInterestMapper.INSTANCE.toDTOList(
                    pointOfInterestRepository.findAllByProfilId(profilDTO.getId())));

            for (PointOfInterestDTO pointOfInterestDTO : profilDTO.getPointOfInterestDTOList()) {

                /**
                 * OBTENIR LES TAGS BY ID POINT OF INTEREST
                 * @see TagRepository#findAllTagByPointOfInterest(Integer)
                 */
                pointOfInterestDTO.setTagDTOList(TagMapper.INSTANCE.toDTOList(
                        tagRepository.findAllTagByPointOfInterest(pointOfInterestDTO.getId())));
            }

            /**
             * OBTENIR LES SOCIAL NETWORK BY ID PROFIL
             * @see SocialNetworkRepository#findAllByProfilId(Integer)
             */
            profilDTO.setSocialNetworkDTOList(SocialNetworkMapper.INSTANCE.toDTOList(
                    socialNetworkRepository.findAllByProfilId(profilDTO.getId())));

        } catch (Exception pEX) {
            logger.error("{}", String.valueOf(pEX));
        }
        return profilDTO;
    }


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* ------------------------------------------ ADD PROFIL ------------------------------------------ */
    @ApiOperation(value = "ADD PROFIL")
    @PostMapping(value = "/profil/profil/add-profil")
    public ProfilDTO addProfil(@RequestBody ProfilDTO profilDTO) {

        /**
         * VERIFIE SI LE PROFIL EXISTE DEJA POUR LE MEME EMAIL OU LE MEME NUMERO DE TELEPHONE
         * @see ProfilRepository#profilExistsByEmail(String)
         */
        Boolean profilExistsByEmail = profilRepository.profilExistsByEmail(profilDTO.getEmail());
        Boolean profilExistsByPhoneNumber = profilRepository.profilExistsByPhoneNumber(profilDTO.getPhoneNumber());

        if (!profilExistsByEmail && !profilExistsByPhoneNumber) {

            try {

                profilDTO.setId(ProfilMapper.INSTANCE.toDTO(profilRepository.save(
                        ProfilMapper.INSTANCE.toEntity(profilDTO))).getId());

            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }

        } else {

            if (profilExistsByEmail && profilExistsByPhoneNumber)
                throw new ProfilConflictException("The email '" + profilDTO.getEmail() + "' and phone number '" + profilDTO.getPhoneNumber() +
                        "' you entered are already in use");

            if (profilExistsByEmail)
            throw new ProfilConflictException("The email '" + profilDTO.getEmail() + "' you entered is already in use");

            if (profilExistsByPhoneNumber)
                throw new ProfilConflictException("The phone number '" + profilDTO.getPhoneNumber() + "' you entered is already in use");
        }
        return profilDTO;
    }


                                        /* ===================================== */
                                        /* ============== UPDATE =============== */
                                        /* ===================================== */

    /* -------------------------------------------- UP PROFIL -------------------------------------------- */
    @ApiOperation(value = "UP PROFIL")
    @PutMapping(value = "/profil/profil/up-profil")
    public ProfilDTO upProfil(@RequestBody ProfilDTO profilDTO) {

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilDTO.getId());

        if (profilExists) {

            try {

                profilDTO = ProfilMapper.INSTANCE.toDTO(profilRepository.save(ProfilMapper.INSTANCE.toEntity(profilDTO)));

            }catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }

        } else {
            logger.error("Update failure - profil : '" + profilDTO.getLastName() +" "+ profilDTO.getFirstName() + "' - Is not found");
            throw new ProfilNotFoundException("Update failure - profil : '" + profilDTO.getLastName() +" "+ profilDTO.getFirstName() +
                    "' - Is not found");
        }
        return profilDTO;
    }


                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* ------------------------------------------ DEL PROFIL ------------------------------------------ */
    @ApiOperation(value = "DEL FULL PROFIL")
    @DeleteMapping(value = "/profil/profil/del-full-profil/{profilId}")
    public String delFullProfil(@PathVariable Integer profilId) {

        String massageStatus = "";

        try {

            /** @see ProfilRepository#deleteProfilById(Integer) */
            Integer check = profilRepository.deleteProfilById(profilId);

            if (check == 0) {
                massageStatus = "Failed deletion - profil not found";
            } else {
                massageStatus = "Successful deletion of the profil";
            }
        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

}
